var map = L.map('map').setView([51.505, -0.09], 0);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);


var color = "";
for (i = 0; i < coordonate.length; i++) {

    if (coordonate[i][2] <= 3) {
        color = "green"
    } else if (coordonate[i][2] < 6) {
        color = "lightcoral"
    } else if (coordonate[i][2] < 9) {
        color = "orange"
    } else {
        color = "red";
    }


    var circle = L.circle([coordonate[i][0], coordonate[i][1]], {
        color: color,
        fillColor: color,
        fillOpacity: 0.5,
        radius: 1000
    }).addTo(map);
}

function go(longitude, latitude) {
    map.setView([longitude, latitude], 12);
}

