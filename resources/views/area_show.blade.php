@extends('welcome')

@section('content')
    <?php
    $coordonate = [];
    $dangerLVL = [];
    ?>

    <div class="maincontent">
        <table class="table">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Danger level</th>
                    <th>longitude</th>
                    <th>Latitude</th>
                    <th>Date</th>
                    <th>Minerals</th>

                </tr>
            </thead>
            <tboby>
                @foreach ($area as $a)
                    <tr>
                        <td>
                            <a href="/area/update/{{ $a->id }}">update</a>
                            <a href="/area/delete/{{ $a->id }}">delete</a>
                        </td>
                    </tr>
                    <tr>
                        <td>{{ $a->name }}</td>
                        <td>{{ $a->danger }}</td>
                        <td>{{ $a->longitude }}</td>
                        <td>{{ $a->latitude }}</td>
                        <td>{{ $a->date }}</td>
                        <td>{{ $a->minerals->name }}</td>
                        <td>
                            <button class="bouton" <?php echo 'onclick=' . 'go(' . $a->longitude . ',' . $a->latitude . ')'; ?>>GO</button>
                        </td>
                        <?php
                        array_push($coordonate, [$a->longitude, $a->latitude, $a->danger]);
                        ?>
                    </tr>
                @endforeach

            </tboby>
        </table>
        <a class="btn btn-success" href="/areaForm">New area</a>
    </div>
    <div class="maincontent" id="map">
    </div>
    <script>
        var coordonate = []
        @foreach ($coordonate as $coord)
            coordonate.push(<?php echo '[' . $coord[0] . ',' . $coord[1] . ',' . $coord[2] . ']' . ','; ?>)
        @endforeach
    </script>
@endsection
