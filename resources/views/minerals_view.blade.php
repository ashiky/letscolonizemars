@extends('welcome')

@section('content')
    <div class="maincontent">
        <table class="table">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tboby>
                @foreach ($minerals as $mineral)
                    <tr>

                        <td>{{ $mineral->name }}</td>
                        <td>{{ $mineral->quantite }} T</td>

                    </tr>
                    <tr class="effects">
                        <th>Effects</th>
                        <td>{{ $mineral->effects }}</td>
                    </tr>
                    <tr>
                        <td>
                            <a class="bouton" href="/minerals/update/{{ $mineral->id }}">update</a>
                            <a class="bouton" href="/minerals/delete/{{ $mineral->id }}">delete</a>
                        </td>
                    </tr>
                @endforeach
            </tboby>
        </table>
        <a class="btn btn-success" href="/mineralsForm">New minerals</a>
    </div>
@endsection
