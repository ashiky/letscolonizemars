@extends('welcome')

@section('content')
    <div class="maincontent">
        <form class="formArea" name="areaForm" method="post" action="/area/{{ $path }}">
            <a class="btn btn-success" href="/area">Retour</a>
            @csrf
            <div class="inputForm">
                <label for="name" class="form-label">Area name</label>
                <input name="name" id="name" class="" value="{{ $area->name ?? '' }}">
            </div>
            <div class="inputForm">
                <label for="danger" class="form-label">Danger level</label>
                <input name="danger" id="danger" class="" value="{{ $area->danger ?? '' }}">
            </div>
            <div class="inputForm">
                <select name="minerals" id="minerals">
                    @foreach ($minerals as $mineral)
                        <option value="{{ $mineral->id }}" @if (isset($area) && $mineral->id == $area->minerals_id) selected @endif>
                            {{ $mineral->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="inputForm">
                <label for="date" id="date" class="form-label">Date</label>
                <input name="date" id="date" class="" value="{{ $area->date ?? '' }}">
            </div>
            <div class="inputForm">
                <label for="longitude" id="longitude" class="form-label">Longitude</label>
                <input name="longitude" id="longitude" class="" value="{{ $area->longitude ?? '' }}">
            </div>
            <div class="inputForm">
                <label for="latitude" id="latitude" class="form-label">Latitude</label>
                <input name="latitude" id="latitude" class="" value="{{ $area->latitude ?? '' }}">
            </div>
            <div class="inputForm">
                <input type="hidden" name="id" value="{{ $area->id ?? '' }}">
                <button type="submit" class="btn btn-info">Valider</button>

            </div>
        </form>
    </div>
@endsection
