@extends('welcome')

@section('content')
    <div class="maincontent">
        <form class="formArea" name="mineralsForm" method="post" action="/minerals/{{ $path }}">
            <a class="btn btn-success" href="/minerals">Retour</a>
            @csrf
            <div class="inputForm">
                <label for="name" class="form-label">Minerals name</label>
                <input name="name" id="name" class="" value="{{ $minerals->name ?? '' }}">
            </div>
            <div class="inputForm">
                <label for="quantite" class="form-label">Minerals quantite</label>
                <input name="quantite" id="quantite" class="" value="{{ $minerals->quantite ?? '' }}">
            </div>
            <div class="inputForm">
                <label for="effects" class="form-label">Minerals effects</label>
                <input name="effects" id="effects" class="" value="{{ $minerals->effects ?? '' }}">
            </div>
            <div class="inputForm">
                <input type="hidden" name="id" value="{{ $minerals->id ?? '' }}">
                <button type="submit" class="btn btn-info">Valider</button>
            </div>
        </form>
    </div>
@endsection
