<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('minerals', function (Blueprint $table) {
            $table->id('id');
            $table->timestamps();
            $table->string('name');
            $table->string('effects');
            $table->decimal('quantite');
        });
        Illuminate\Support\Facades\DB::table('minerals')->insert(
            array(
                'id' => '1',
                'name' => 'Klingon',
                'effects' => 'Un peu relou, altère les combinaisons, traverse les matériaux et provoque des démangeaisons cutanées, On a observé une résistance chez les geeks',
                'quantite'=> 100

            )
        );
        Illuminate\Support\Facades\DB::table('minerals')->insert(
            array(
                'id' => '2',
                'name' => 'Chomdû',
                'effects' => 'Le Chomdû est un minerai qui provoque des états dépressifs. Bizzarement, les personnes âgées ne sont plus affectées...',
                'quantite'=> 100
            )
        );
        Illuminate\Support\Facades\DB::table('minerals')->insert(
            array(
                'id' => '3',
                'name' => 'Perl',
                'effects' => 'Le Perl est le minerai à éviter à tout prix ! Hystérie, dépression, folie, voir mort subite sont les effets fréquemment observés.',
                'quantite'=> 100

            )
        );
    }
    


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('minerals');
    }
};
