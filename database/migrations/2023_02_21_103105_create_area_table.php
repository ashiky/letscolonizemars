<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('area', function (Blueprint $table) {
            $table->id("id");
            $table->timestamps();
            $table->string('name');
            $table->string('danger');
            $table->foreignId('minerals_id')->constrained('minerals')->onDelete('cascade');;
            $table->string('date');
            $table->decimal('longitude');
            $table->decimal('latitude');
        });
        Illuminate\Support\Facades\DB::table('area')->insert(
            array(
                'id' => '1',
                'name' => 'Iot Valley',
                'danger' => '1',
                'minerals_id' => '1',
                'date' => '12/03/2248',
                'longitude' => '43.5451',
                'latitude' => '1.51828'
            )
        );
        Illuminate\Support\Facades\DB::table('area')->insert(
            array(
                'id' => '2',
                'name' => 'Oloron Saint Marie',
                'danger' => '10',
                'minerals_id' => '3',
                'date' => '12/03/2248',
                'longitude' => '43.1679',
                'latitude' => '-0.7155'
            )
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('area');
    }
};
