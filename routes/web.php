<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\MineralsController;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', [AreaController::class, 'list']);
Route::get('/area', [AreaController::class, 'list']);
Route::get('/areaForm', [AreaController::class, 'create']);
Route::post('/area/create', [AreaController::class, 'create']);
Route::get('/area/update/{area_id}', [AreaController::class, 'update']);
Route::post('/area/update', [AreaController::class, 'update']);
Route::get('/area/delete/{id}', [AreaController::class, 'delete']);

Route::get('/minerals', [MineralsController::class, 'list']);
Route::get('/mineralsForm', [MineralsController::class, 'create']);
Route::post('/minerals/create', [MineralsController::class, 'create']);
Route::get('/minerals/update/{minerals_id}', [MineralsController::class, 'update']);
Route::post('/minerals/update', [MineralsController::class, 'update']);
Route::get('/minerals/delete/{id}', [MineralsController::class, 'delete']);

