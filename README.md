This application is made to list all the dangerous area contaminated by minerals.

	Before doing anithing,create an .env and connect it to your data base 
	then do the commands below:
		-npm install
			(make sure you have all the projects package)
		-php artisan migrate
			(create the data base with the migration)
		-php artisan serve
			(run the server and give you the link to the page)

All the text in purple is a button or a link:
	-"update" with this button you can update the content next to it
	-"delete"  with this button you can delete the content next to it
	-"New area" and "New minerals"  with this button you can create new content
		(don't forget to create a new minerals before adding a new area)
	-"Area" make you move to the area page, here you can see 2 differentes parts:
		 the left part is a table of area with some data:
			-"Name" the name of the area
			-"Danger level" the danger lanvel (1-10)
			-"Longitude" "Latitude" the coordonate of the area (the coordonate goes to 0 at 100)
			-"date" the date of the discoveri of the area 
			-"minerals" the mineral within the area
			-"GO" a button to see the area on the map
		the right part is a map of mars (everyone know that mars in 2248 is a copy of the Earth) :
			-here you can move on the map and see all the dangerous area
			-the circle represent the dangerous area, the color of the circle represent their dangerosity:
				-green = danger level under 3
				-lightcoral = danger level between 3 and 6
				-orange = danger level between 6 and 9
				-orange = danger level above 10
	-"Minerals" make you move to the minerals page, here you can see all the differents minerals and their effects
