<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Minerals;
class MineralsController extends Controller {
    public function list()
    {
        $minerals = Minerals::all();
        return view('minerals_view', compact('minerals'));
    }
    public function create(Request $request)
    {
        if ($request->input('name') !== null) {
            $minerals = new Minerals;
            $minerals->name = $request->input('name');
            $minerals->quantite = $request->input('quantite');
            $minerals->effects = $request->input('effects');
            $minerals->save();
            return redirect('/minerals');
        }
        return view('minerals_form', ['path' => 'create']);
    }
    public function update(Request $request, $id = null)
    {
        if ($request->input('name') !== null) {
            echo("minerals");
            $minerals = Minerals::find($request->input('id'));
            $minerals->name = $request->input('name');
            $minerals->quantite = $request->input('quantite');
            $minerals->effects = $request->input('effects');
            $minerals->save();
            return redirect('/minerals');
        }
        $minerals = Minerals::find($id);
        return view('minerals_form', ['path' => 'update', 'minerals' => $minerals]);
    }
    public function delete($id)
    {
        $minerals = Minerals::find($id);
        $minerals->delete();
        return redirect('/minerals');
    }
}
