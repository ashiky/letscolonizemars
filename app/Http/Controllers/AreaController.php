<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Minerals;
class AreaController extends Controller
{

    public function list()
    {
        $area = Area::with('minerals')->get();
        return view('area_show', compact('area'));
    }


    public function show($id)
    {
        $minerals = Minerals::find($id);
        $area = Area::where('minerals_id', $id)->with('minerals')->get();
        return view('area_show', ['libraries' => $area, 'area' => $area]);
    }

    public function create(Request $request)
    {
        if ($request->input('name') !== null) {
            $area = new Area;
            $area->name = $request->input('name');
            $area->danger = $request->input('danger');
            $area->date = $request->input('date');
            $area->minerals_id = $request->input('minerals');
            $area->latitude = $request->input('latitude');
            $area->longitude = $request->input('longitude');
            
    
            $area->save();
            return redirect('/area');
        }
        $minerals = Minerals::all();
        return view('area_form', ['path' => 'create','minerals' => $minerals]);
    }

    public function update(Request $request, $id = null)
    {
        if ($request->input('name') !== null) {
            $area = Area::find($request->input('id'));
            $area->name = $request->input('name');
            $area->danger = $request->input('danger');
            $area->date = $request->input('date');
            $area->minerals_id = $request->input('minerals');
            $area->latitude = $request->input('latitude');
            $area->longitude = $request->input('longitude');
            $area->save();
            return redirect('/area');
        }
        $area = Area::find($id);
        $minerals = Minerals::all();
        return view('area_form', ['path' => 'update', 'area' => $area ,'minerals' => $minerals]);
    }

    public function delete($id)
    {
        $area = Area::find($id);
        $area->delete();
        return redirect('/area');
    }


}
