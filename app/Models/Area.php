<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use HasFactory;

    protected $table = "area";

    public function listArea()
    {
        return $this->hasMany(Area::class, 'area_id');
    }
    public function minerals()
    {
        return $this->belongsTo(Minerals::class);
    }
}
