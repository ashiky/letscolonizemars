<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Minerals extends Model
{
    use HasFactory;

    protected $table = "minerals";

    public function listMinerals()
    {
        return $this->hasMany(Minerals::class, 'minerals_id');
    }
}
